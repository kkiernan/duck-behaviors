<?php

namespace Acme\QuackBehaviors;

class Squeak implements QuackBehavior
{
    public function quack()
    {
        print("Squeak!\n");
    }
}
