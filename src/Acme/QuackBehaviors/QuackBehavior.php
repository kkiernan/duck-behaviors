<?php

namespace Acme\QuackBehaviors;

interface QuackBehavior
{
    public function quack();
}
