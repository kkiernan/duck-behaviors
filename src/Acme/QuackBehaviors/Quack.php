<?php

namespace Acme\QuackBehaviors;

class Quack implements QuackBehavior
{
    public function quack()
    {
        print("Quack\n");
    }
}
