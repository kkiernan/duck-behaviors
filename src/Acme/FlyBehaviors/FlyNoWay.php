<?php

namespace Acme\FlyBehaviors;

class FlyNoWay implements FlyBehavior
{
    public function fly()
    {
        print("I can't fly :(\n");
    }
}
