<?php

namespace Acme\FlyBehaviors;

interface FlyBehavior
{
    public function fly();
}