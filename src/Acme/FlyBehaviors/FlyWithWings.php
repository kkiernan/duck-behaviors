<?php

namespace Acme\FlyBehaviors;

class FlyWithWings implements FlyBehavior
{
    public function fly()
    {
        print("I'm flying!!\n");
    }
}
