<?php

namespace Acme\Ducks;

use Acme\FlyBehaviors\FlyWithWings;
use Acme\QuackBehaviors\Quack;

class Mallard extends Duck
{
    /**
     * Create a new mallard instance.
     */
    public function __construct()
    {
        $this->flyBehavior = new FlyWithWings;
        $this->quackBehavior = new Quack;
    }

    /**
     * Print the type of duck to the screen.
     * 
     * @return void
     */
    public function display()
    {
        print("I am a mallard\n");
    }
}
