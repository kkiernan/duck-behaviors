<?php

namespace Acme\Ducks;

use Acme\FlyBehaviors\FlyNoWay;
use Acme\QuackBehaviors\Squeak;

class RubberDucky extends Duck
{
    /**
     * Create a new rubber ducky instance.
     */
    public function __construct()
    {
        $this->flyBehavior = new FlyNoWay;
        $this->quackBehavior = new Squeak;
    }

    /**
     * Print the duck type to the screen.
     * 
     * @return void
     */
    public function display()
    {
        print("I am a rubber ducky\n");
    }
}
