<?php

namespace Acme\Ducks;

use Acme\FlyBehaviors\FlyBehavior;

abstract class Duck
{
    /**
     * @var Acme\FlyBehaviors\FlyBehavior
     */
    protected $flyBehavior;

    /**
     * @var Acme\QuackBehaviors\QuackBehavior
     */
    protected $quackBehavior;

    /**
     * Perform the fly behavior.
     * 
     * @return void
     */
    public function performFly()
    {
        $this->flyBehavior->fly();
    }

    /**
     * Perform the quack behavior.
     * 
     * @return void
     */
    public function performQuack()
    {
        $this->quackBehavior->quack();
    }

    /**
     * Print a string of the duck type.
     * 
     * @return void
     */
    public function display()
    {
        print("I am a duck\n");
    }

    /**
     * Set the fly behavior.
     * 
     * @param FlyBehavior $flyBehavior The fly behavior implementation.
     * 
     * @return void
     */
    public function setFlyBehavior(FlyBehavior $flyBehavior)
    {
        $this->flyBehavior = $flyBehavior;
    }

}
