<?php

use Acme\Ducks\Mallard;
use Acme\Ducks\RubberDucky;

require '../vendor/autoload.php';

$mallard = new Mallard;
$mallard->display();
$mallard->performQuack(); // Quack
$mallard->performFly(); // I'm flying!!

$rubberDucky = new RubberDucky;
$rubberDucky->display();
$rubberDucky->performQuack(); // Squeak!
$rubberDucky->performFly(); // I can't fly :(
